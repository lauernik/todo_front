export interface ITodoItem {
  id: number;
  completed: boolean;
  title: string;
  tag: string;
}

export interface ITodoList {
  todos: ITodoItem[];
}
