import React from "react";
import { Grid, TextField, Button, ButtonGroup } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { filterTodos, addTodo, filterTodosByTag } from "../../Store/action";
import { FILTER_TYPES, IAddTodo } from "../../Store/types";
import { Rootstate } from "../../Store/reduces";

const TopBar: React.FC<{}> = () => {
  const [title, setTitle] = React.useState<string>("");
  const [tag, setTag] = React.useState<string>("");

  const NewTitleTag: IAddTodo = {
    title: title,
    tag: tag,
  };

  const dispatch = useDispatch();

  const handleAddTaskSubmit = () => {
    if (title) {
      dispatch(addTodo(NewTitleTag));
      setTitle("");
      setTag("");
    }
  };

  const handleTodoAll = React.useCallback(
    () => dispatch(filterTodos(FILTER_TYPES.SHOW_ALL)),
    [dispatch]
  );
  const handleTodoCompleted = React.useCallback(
    () => dispatch(filterTodos(FILTER_TYPES.SHOW_COMPLETED)),
    [dispatch]
  );
  const handleTodoActive = React.useCallback(
    () => dispatch(filterTodos(FILTER_TYPES.SHOW_ACTIVE)),
    [dispatch]
  );
  const handleTagReset = React.useCallback(
    () => dispatch(filterTodosByTag(undefined)),
    [dispatch]
  );

  const filterTag = useSelector(
    (state: Rootstate) => state.todoReducer.filterTag
  );
  const filterTagStatus = (todosFilter: string | undefined) => {
    if (typeof todosFilter === "string") {
      return false;
    } else {
      return true;
    }
  };
  return (
    <Grid container direction="row" justify="center">
      <Grid item>
        <form>
          <TextField
            required
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setTitle(event.currentTarget.value)
            }
            value={title}
            label="Новая задача"
          />
          <TextField
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setTag(event.currentTarget.value)
            }
            value={tag}
            label="Тег"
          />
          <Button variant="contained" onClick={handleAddTaskSubmit}>
            Добавить
          </Button>
        </form>
        <ButtonGroup>
          <Button onClick={handleTodoAll}>Все</Button>
          <Button onClick={handleTodoActive}>Текущие</Button>
          <Button onClick={handleTodoCompleted}>Завершенные</Button>
        </ButtonGroup>
        <Button disabled={filterTagStatus(filterTag)} onClick={handleTagReset}>
          Сбросить фильтр
        </Button>
      </Grid>
    </Grid>
  );
};

export default TopBar;
