import React from "react";
import TodoItem from "./todoItem";
import { List } from "@material-ui/core";
import { Rootstate } from "../../Store/reduces";
import { useSelector, useDispatch } from "react-redux";
import {
  deleteTodo,
  changeCheckbox,
  filterTodosByTag,
  fetchTodo,
} from "../../Store/action";
import { FILTER_TYPES } from "../../Store/types";
import { ITodoItem } from "./types";

const getTodosFilter = (
  todos: ITodoItem[],
  filter: FILTER_TYPES,
  filterTag: string | undefined
) => {
  if (typeof filterTag === "string") {
    todos = todos.filter((item) => item.tag === filterTag);
  }
  switch (filter) {
    case FILTER_TYPES.SHOW_ALL:
      return todos;
    case FILTER_TYPES.SHOW_ACTIVE:
      const activeTodos = todos.filter((item) => !item.completed);
      return activeTodos;
    case FILTER_TYPES.SHOW_COMPLETED:
      const completedTodos = todos.filter((item) => item.completed);
      return completedTodos;

    default:
      return todos;
  }
};

const TodoList: React.FC<{}> = () => {
  const todosAll = useSelector((state: Rootstate) => state.todoReducer.todos);
  const filter = useSelector((state: Rootstate) => state.todoReducer.filter);
  const filterTag = useSelector(
    (state: Rootstate) => state.todoReducer.filterTag
  );

  const todos = getTodosFilter(todosAll, filter, filterTag);
  const dispatch = useDispatch();
  React.useEffect(() => {
    dispatch(fetchTodo());
  }, [dispatch]);
  const handleTaskDelete = React.useCallback(
    (id: number) => dispatch(deleteTodo(id)),
    [dispatch]
  );
  const handleChangeCheckbox = React.useCallback(
    (id: number) => dispatch(changeCheckbox(id)),
    [dispatch]
  );
  const handleFilterTodosByFilter = React.useCallback(
    (tag: string) => dispatch(filterTodosByTag(tag)),
    [dispatch]
  );
  return (
    <div>
      <List>
        {todos.map((todos, index) => (
          <TodoItem
            id={todos.id}
            completed={todos.completed}
            title={todos.title}
            tag={todos.tag}
            key={index}
            onTaskDelete={() => handleTaskDelete(todos.id)}
            onChangeCheckbox={() => handleChangeCheckbox(todos.id)}
            onFilterTag={() => handleFilterTodosByFilter(todos.tag)}
          ></TodoItem>
        ))}
      </List>
    </div>
  );
};

export default TodoList;
