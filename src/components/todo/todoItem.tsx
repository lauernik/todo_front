import React from "react";
import { ITodoItem } from "./types";
import {
  ListItem,
  ListItemText,
  Checkbox,
  Chip,
  IconButton,
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const TodoItem: React.FC<
  ITodoItem & {
    onTaskDelete: (id: number) => void;
    onChangeCheckbox: (id: number) => void;
    onFilterTag: (tag: string) => void;
  }
> = ({
  id,
  completed,
  title,
  tag,
  onTaskDelete,
  onChangeCheckbox,
  onFilterTag,
}) => {
  return (
    <ListItem button>
      <Checkbox checked={completed} onChange={() => onChangeCheckbox(id)} />
      <ListItemText primary={title} />
      <Chip label={tag} onClick={() => onFilterTag(tag)} />

      <IconButton onClick={() => onTaskDelete(id)}>
        <DeleteIcon />
      </IconButton>
    </ListItem>
  );
};

export default TodoItem;
