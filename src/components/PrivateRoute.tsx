import React from "react";
import { Route, Redirect, RouteProps } from "react-router-dom";

const PrivateRoute: React.FC<{} & RouteProps> = ({ children, ...rest }) => {
  return (
    <div>
      {localStorage.getItem("token") ? (
        <Route {...rest}>{children}</Route>
      ) : (
        <Redirect exact to="/login" />
      )}
    </div>
  );
};

export default PrivateRoute;
