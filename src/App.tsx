import React from "react";

import { HomePage, LoginPage, RegistrationPage } from "./pages";
import { CssBaseline } from "@material-ui/core";
import { Route } from "react-router-dom";
import { PrivateRoute } from "./components";

function App() {
  return (
    <div className="App">
      <CssBaseline />
      <PrivateRoute exact path="/">
        <HomePage />
      </PrivateRoute>
      <Route exact path="/login">
        <LoginPage />
      </Route>
      <Route exact path="/register">
        <RegistrationPage />
      </Route>
    </div>
  );
}

export default App;
