import { ITodosState, Actions, FILTER_TYPES } from "./types";
import {
  DEL_TODO,
  CHANGE_CHECKBOX,
  SET_FILTER,
  SET_FILTER_TAG,
  FETCH_TODO,
} from "./constant";

import { combineReducers } from "redux";

const initState: ITodosState = {
  todos: [],
  filter: FILTER_TYPES.SHOW_ALL,
  filterTag: undefined,
};

const todoReducer = (state = initState, action: Actions): ITodosState => {
  switch (action.type) {
    case DEL_TODO:
      const filterTodos = state.todos.filter(
        (item) => item.id !== action.payload
      );
      return { ...state, todos: filterTodos };
    case CHANGE_CHECKBOX:
      const checkCheckbox = state.todos.map((item) =>
        item.id === action.payload
          ? { ...item, completed: !item.completed }
          : item
      );
      return { ...state, todos: checkCheckbox };
    case SET_FILTER:
      return { ...state, filter: action.payload };
    case SET_FILTER_TAG:
      return { ...state, filterTag: action.payload };
    case FETCH_TODO:
      return { ...state, todos: action.payload };
    default:
      return state;
  }
};

export const reduceres = combineReducers({
  todoReducer,
});

export type Rootstate = ReturnType<typeof reduceres>;
