export const SET_FILTER = "SET_FILTER";
export const DEL_TODO = "DEL_TODO";
export const CHANGE_CHECKBOX = "CHANGE_CHECKBOX";
export const SET_FILTER_TAG = "SET_FILTER_TAG";
export const FETCH_TODO = "FETCH_TODO";
