import { Actions, IAddTodo, FILTER_TYPES } from "./types";
import {
  DEL_TODO,
  CHANGE_CHECKBOX,
  SET_FILTER,
  SET_FILTER_TAG,
  FETCH_TODO,
} from "./constant";
import { Dispatch } from "react";
import { IUserLogin } from "../pages/LoginPage";
import API from "./api";
import { Rootstate } from "./reduces";
import { IUserRegister } from "../pages/RegistrationPage";

const api = new API();

export const addTodo = (task: IAddTodo) => async (
  dispatch: Dispatch<Actions>
) => {
  await api.addTodo(task);
  const todos = await api.fetchTodo();
  dispatch({
    type: FETCH_TODO,
    payload: todos,
  });
};

export const deleteTodo = (id: number) => async (
  dispatch: Dispatch<Actions>
) => {
  await api.deleteTodo(id);
  dispatch({
    type: DEL_TODO,
    payload: id,
  });
};

export const changeCheckbox = (id: number) => async (
  dispatch: Dispatch<Actions>,
  getState: () => Rootstate
) => {
  const statusTodo = getState().todoReducer.todos.find((item) => item.id === id)
    ?.completed;
  await api.changeCheckbox(id, statusTodo ? false : true);
  dispatch({
    type: CHANGE_CHECKBOX,
    payload: id,
  });
};

export const filterTodos = (filter: FILTER_TYPES): Actions => ({
  type: SET_FILTER,
  payload: filter,
});

export const filterTodosByTag = (tag: string | undefined): Actions => ({
  type: SET_FILTER_TAG,
  payload: tag,
});

export const setToken = (userData: IUserLogin) => async (
  dispatch: Dispatch<Actions>
) => {
  const token = await api.getToken(userData);
  localStorage.setItem("token", token);
};

export const fetchTodo = () => async (dispatch: Dispatch<Actions>) => {
  const todos = await api.fetchTodo();
  dispatch({
    type: FETCH_TODO,
    payload: todos,
  });
};

export const addUser = (userData: IUserRegister) => async (
  dispatch: Dispatch<Actions>
) => {
  await api.addUser(userData);
};
