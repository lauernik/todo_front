import {
  DEL_TODO,
  CHANGE_CHECKBOX,
  SET_FILTER,
  SET_FILTER_TAG,
  FETCH_TODO,
} from "./constant";
import { ITodoItem } from "../components/todo/types";

export interface ITodosState {
  todos: ITodoItem[];
  filter: FILTER_TYPES;
  filterTag: undefined | string;
}

export interface IAddTodo {
  title: string;
  tag: string;
}

interface IDelTodoAction {
  type: typeof DEL_TODO;
  payload: number;
}

interface IChangeCheckboxAction {
  type: typeof CHANGE_CHECKBOX;
  payload: number;
}

export enum FILTER_TYPES {
  SHOW_ALL,
  SHOW_ACTIVE,
  SHOW_COMPLETED,
}

interface IFilterTodosAction {
  type: typeof SET_FILTER;
  payload: FILTER_TYPES;
}

interface IFilterTodosByTagAction {
  type: typeof SET_FILTER_TAG;
  payload: string | undefined;
}

interface IFetchTodoAction {
  type: typeof FETCH_TODO;
  payload: ITodoItem[];
}

export type Actions =
  | IDelTodoAction
  | IChangeCheckboxAction
  | IFilterTodosAction
  | IFilterTodosByTagAction
  | IFetchTodoAction;
