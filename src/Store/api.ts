import axios, { AxiosInstance } from "axios";
import { IUserLogin } from "../pages/LoginPage";
import { IUserRegister } from "../pages/RegistrationPage";
import { ITodoItem } from "../components/todo/types";
import { IAddTodo } from "./types";

class API {
  private axios: AxiosInstance;
  constructor() {
    this.axios = axios.create({
      baseURL: "http://localhost:8000",
    });
  }
  public async getToken(userData: IUserLogin): Promise<string> {
    try {
      const response = await this.axios.post("/auth/token", userData);
      return response.data.token;
    } catch {
      return "";
    }
  }
  public async fetchTodo(): Promise<ITodoItem[]> {
    const token = localStorage.getItem("token");
    const response = await this.axios.get("/todos", {
      headers: {
        Authorization: `Token ${token}`,
      },
    });
    return response.data;
  }
  public async addTodo(todo: IAddTodo): Promise<void> {
    const token = localStorage.getItem("token");
    await this.axios.post(
      "/todos/",
      { title: todo.title, tag: todo.tag },
      {
        headers: {
          Authorization: `Token ${token}`,
        },
      }
    );
  }
  public async deleteTodo(id: number): Promise<void> {
    const token = localStorage.getItem("token");
    await this.axios.delete(`/todos/${id}`, {
      headers: {
        Authorization: `Token ${token}`,
      },
    });
  }
  public async changeCheckbox(id: number, checkbox: boolean): Promise<void> {
    const token = localStorage.getItem("token");
    await this.axios.patch(
      `/todos/${id}`,
      { completed: checkbox },
      {
        headers: {
          Authorization: `Token ${token}`,
        },
      }
    );
  }
  public async addUser(userData: IUserRegister): Promise<void> {
    await this.axios.post("register/", {
      username: userData.username,
      password: userData.password,
      password2: userData.password2,
    });
  }
}

export default API;
