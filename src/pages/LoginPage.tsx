import React from "react";
import {
  Typography,
  CssBaseline,
  TextField,
  Box,
  Button,
  Grid,
} from "@material-ui/core";
// import { useDispatch } from "react-redux";
import { useHistory, withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setToken } from "../Store/action";

export interface IUserLogin {
  username: string;
  password: string;
}

const LoginPage: React.FC<{}> = () => {
  const [user, setUser] = React.useState<string>("");
  const [pswrd, setPswrd] = React.useState<string>("");

  const userPassword: IUserLogin = {
    username: user,
    password: pswrd,
  };

  const dispatch = useDispatch();
  const history = useHistory();

  const handleSumbit = () => {
    dispatch(setToken(userPassword));
    history.push("/");
  };

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: "100vh" }}
    >
      <CssBaseline />
      <div>
        <Typography component="h1" variant="h5">
          Login Page
        </Typography>
        <form noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Username"
            autoFocus
            value={user}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setUser(event.currentTarget.value)
            }
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Password"
            type="password"
            autoComplete="current-password"
            value={pswrd}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setPswrd(event.currentTarget.value)
            }
          />
          <Button
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleSumbit}
          >
            Вход
          </Button>
          <Button fullWidth onClick={() => history.push("/register")}>
            Регистрация
          </Button>
        </form>
      </div>
      <Box mt={8}></Box>
    </Grid>
  );
};

export default withRouter(LoginPage);
