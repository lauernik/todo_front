import React from "react";
import TodoList from "../components/todo/todolist";
import { AppBar, Toolbar, Typography, Grid, Button } from "@material-ui/core";
import { TopBar } from "../components";
import { withRouter, useHistory } from "react-router-dom";

const HomePage: React.FC<{}> = () => {
  const history = useHistory();
  const handleExit = () => {
    localStorage.removeItem("token");
    history.push("/login");
  };
  return (
    <div>
      <Grid>
        <Grid item>
          <AppBar position="static">
            <Toolbar>
              <Grid container justify="flex-start">
                <Typography variant="h6">Список задач</Typography>
              </Grid>
              <Grid container justify="flex-end">
                <Button
                  variant="contained"
                  color="secondary"
                  onClick={handleExit}
                >
                  Выход
                </Button>
              </Grid>
            </Toolbar>
          </AppBar>
        </Grid>
      </Grid>
      <Grid container justify="center">
        <TopBar />
        <Grid item md={6} xs={12}>
          <TodoList />
        </Grid>
      </Grid>
    </div>
  );
};

export default withRouter(HomePage);
