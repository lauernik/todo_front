import React from "react";
import {
  Typography,
  CssBaseline,
  TextField,
  Box,
  Button,
  Grid,
} from "@material-ui/core";
// import { useDispatch } from "react-redux";
import { useHistory, withRouter } from "react-router-dom";
import { useDispatch } from "react-redux";
import { addUser } from "../Store/action";

export interface IUserRegister {
  username: string;
  password: string;
  password2: string;
}

const RegistrationPage: React.FC<{}> = () => {
  const [user, setUser] = React.useState<string>("");
  const [pswrd, setPswrd] = React.useState<string>("");
  const [pswrd2, setPswrd2] = React.useState<string>("");

  const userPassword: IUserRegister = {
    username: user,
    password: pswrd,
    password2: pswrd2,
  };

  const dispatch = useDispatch();
  const history = useHistory();

  const handleSumbit = () => {
    dispatch(addUser(userPassword));
    history.push("/login");
  };

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: "100vh" }}
    >
      <CssBaseline />
      <div>
        <Typography component="h1" variant="h5">
          Registration Page
        </Typography>
        <form noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Username"
            value={user}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setUser(event.currentTarget.value)
            }
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Password"
            type="password"
            value={pswrd}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setPswrd(event.currentTarget.value)
            }
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Repeat password"
            type="password"
            value={pswrd2}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              setPswrd2(event.currentTarget.value)
            }
          />
          <Button
            fullWidth
            variant="contained"
            color="primary"
            onClick={handleSumbit}
          >
            Регистрация
          </Button>
        </form>
      </div>
      <Box mt={8}></Box>
    </Grid>
  );
};

export default withRouter(RegistrationPage);
